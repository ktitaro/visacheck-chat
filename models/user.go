package models

import (
	"github.com/gofiber/fiber"
	"github.com/jinzhu/gorm"
)

// User represents application user
type User struct {
	*gorm.Model
	Username string
	Fullname string
	Email    string
	IsAdmin  bool
	IsOnline bool
}

// Map converts `User` into `fiber.Map`
func (m *User) Map() *fiber.Map {
	return &fiber.Map{
		"id":       m.ID,
		"email":    m.Email,
		"username": m.Username,
		"fullname": m.Fullname,
	}
}

// UserRepo represents methods of the `User`
type UserRepo struct {
	db *gorm.DB
}

func newUserRepo(db *gorm.DB) *UserRepo {
	return &UserRepo{db}
}

// Create attempts to create new `User`
func (r *UserRepo) Create(attrs *User) (*User, error) {
	if err := r.db.Create(attrs).Error; err != nil {
		return nil, err
	}
	return attrs, nil
}

// FindAll attempts to find all instances of `User` by attributes
func (r *UserRepo) FindAll(attrs *User) ([]*User, error) {
	var users []*User
	if err := r.db.Where(attrs).Find(&users).Error; err != nil {
		return nil, err
	}
	return users, nil
}

// FindOne attempts to find single instance of `User` by attributes
func (r *UserRepo) FindOne(attrs *User) (*User, error) {
	user := new(User)
	if err := r.db.Where(attrs).First(user).Error; err != nil {
		return nil, err
	}
	return user, nil
}

// FindOneOrCreate attempts to find single instance of `User` by attribute
// otherwise create new instance of `User` with attributes
func (r *UserRepo) FindOneOrCreate(attrs *User) (*User, error) {
	var user *User
	user, err := r.FindOne(attrs)
	if err != nil {
		if !gorm.IsRecordNotFoundError(err) {
			return nil, err
		}
		user, err = r.Create(attrs)
		if err != nil {
			return nil, err
		}
	}
	return user, nil
}

// FindByID attempts to find single instance of `User` by ID
func (r *UserRepo) FindByID(id uint) (*User, error) {
	user := new(User)
	if err := r.db.First(user, id).Error; err != nil {
		return nil, err
	}
	return user, nil
}

// Update attempts to update `User` with attributes
func (r *UserRepo) Update(id uint, attrs *User) (*User, error) {
	user, err := r.FindByID(id)
	if err != nil {
		return nil, err
	}
	if err := r.db.Model(user).Updates(attrs).Error; err != nil {
		return nil, err
	}
	return user, nil
}

// Delete attempts to delete instance of `User`
func (r *UserRepo) Delete(id uint) (*User, error) {
	user, err := r.FindByID(id)
	if err != nil {
		return nil, err
	}
	if err := r.db.Delete(user).Error; err != nil {
		return nil, err
	}
	return user, nil
}
