package models

import (
	"github.com/gofiber/fiber"
	"github.com/jinzhu/gorm"
)

// Country represents country of the world
type Country struct {
	*gorm.Model
	Name        string
	Slug        string
	Region      *Region
	Subregion   *Subregion
	RegionID    uint
	SubregionID uint
}

// Map converts `Country` into `fiber.Map`
func (m *Country) Map() *fiber.Map {
	return &fiber.Map{
		"id":        m.ID,
		"name":      m.Name,
		"slug":      m.Slug,
		"region":    m.RegionID,
		"subregion": m.SubregionID,
	}
}

// CountryRepo represents methods of the `Country`
type CountryRepo struct {
	db *gorm.DB
}

func newCountryRepo(db *gorm.DB) *CountryRepo {
	return &CountryRepo{db}
}

// Create attempts to create new `Country`
func (r *CountryRepo) Create(attrs *Country) (*Country, error) {
	if err := r.db.Create(attrs).Error; err != nil {
		return nil, err
	}
	return attrs, nil
}

// FindAll attempts to find all instances of `Country` by attributes
func (r *CountryRepo) FindAll(attrs *Country) ([]*Country, error) {
	var countries []*Country
	if err := r.db.Where(attrs).Find(&countries).Error; err != nil {
		return nil, err
	}
	return countries, nil
}

// FindOne attempts to find single instance of `Country` by attributes
func (r *CountryRepo) FindOne(attrs *Country) (*Country, error) {
	country := new(Country)
	if err := r.db.Where(attrs).First(country).Error; err != nil {
		return nil, err
	}
	return country, nil
}

// FindByID attempts to find single instance of `Country` by ID
func (r *CountryRepo) FindByID(id uint) (*Country, error) {
	country := new(Country)
	if err := r.db.First(country, id).Error; err != nil {
		return nil, err
	}
	return country, nil
}

// Update attempts to update `Country` with attributes
func (r *CountryRepo) Update(id uint, attrs *Country) (*Country, error) {
	country, err := r.FindByID(id)
	if err != nil {
		return nil, err
	}
	if err := r.db.Model(country).Updates(attrs).Error; err != nil {
		return nil, err
	}
	return country, nil
}

// Delete attempts to delete instance of `Country`
func (r *CountryRepo) Delete(id uint) (*Country, error) {
	country, err := r.FindByID(id)
	if err != nil {
		return nil, err
	}
	if err := r.db.Delete(country).Error; err != nil {
		return nil, err
	}
	return country, nil
}
