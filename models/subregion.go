package models

import (
	"github.com/gofiber/fiber"
	"github.com/jinzhu/gorm"
)

// Subregion represents subregion of the world
type Subregion struct {
	*gorm.Model
	Name     string
	Slug     string
	Region   *Region
	RegionID uint
}

// Map converts `Subregion` into `fiber.Map`
func (m *Subregion) Map() *fiber.Map {
	return &fiber.Map{
		"id":     m.ID,
		"name":   m.Name,
		"slug":   m.Slug,
		"region": m.RegionID,
	}
}

// SubregionRepo represents methods of the `Subregion`
type SubregionRepo struct {
	db *gorm.DB
}

func newSubregionRepo(db *gorm.DB) *SubregionRepo {
	return &SubregionRepo{db}
}

// Create attempts to create new `Subregion`
func (r *SubregionRepo) Create(attrs *Subregion) (*Subregion, error) {
	if err := r.db.Create(attrs).Error; err != nil {
		return nil, err
	}
	return attrs, nil
}

// FindAll attempts to find all instances of `Subregion` by attributes
func (r *SubregionRepo) FindAll(attrs *Subregion) ([]*Subregion, error) {
	var subregions []*Subregion
	if err := r.db.Where(attrs).Find(&subregions).Error; err != nil {
		return nil, err
	}
	return subregions, nil
}

// FindOne attempts to find single instance of `Subregion` by attributes
func (r *SubregionRepo) FindOne(attrs *Subregion) (*Subregion, error) {
	subregion := new(Subregion)
	if err := r.db.Where(attrs).First(subregion).Error; err != nil {
		return nil, err
	}
	return subregion, nil
}

// FindByID attempts to find single instance of `Subregion` by ID
func (r *SubregionRepo) FindByID(id uint) (*Subregion, error) {
	subregion := new(Subregion)
	if err := r.db.First(subregion, id).Error; err != nil {
		return nil, err
	}
	return subregion, nil
}

// Update attempts to update `Subregion` with attributes
func (r *SubregionRepo) Update(id uint, attrs *Subregion) (*Subregion, error) {
	subregion, err := r.FindByID(id)
	if err != nil {
		return nil, err
	}
	if err := r.db.Model(subregion).Updates(attrs).Error; err != nil {
		return nil, err
	}
	return subregion, nil
}

// Delete attempts to delete instance of `Subregion`
func (r *SubregionRepo) Delete(id uint) (*Subregion, error) {
	subregion, err := r.FindByID(id)
	if err != nil {
		return nil, err
	}
	if err := r.db.Delete(subregion).Error; err != nil {
		return nil, err
	}
	return subregion, nil
}
