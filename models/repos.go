package models

import "github.com/jinzhu/gorm"

// Repos combines defined repos
type Repos struct {
	User      *UserRepo
	Region    *RegionRepo
	Country   *CountryRepo
	Subregion *SubregionRepo
}

// New create new intsance of `Repos`
func New(db *gorm.DB) *Repos {
	return &Repos{
		User:      newUserRepo(db),
		Region:    newRegionRepo(db),
		Country:   newCountryRepo(db),
		Subregion: newSubregionRepo(db),
	}
}
