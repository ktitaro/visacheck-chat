package models

import (
	"github.com/gofiber/fiber"
	"github.com/jinzhu/gorm"
)

// Region represents region of the world
type Region struct {
	*gorm.Model
	Name string
	Slug string
}

// Map coverts `Region` into `fiber.Map`
func (m *Region) Map() *fiber.Map {
	return &fiber.Map{
		"id":   m.ID,
		"name": m.Name,
		"slug": m.Slug,
	}
}

// RegionRepo represents methods of the `Region`
type RegionRepo struct {
	db *gorm.DB
}

func newRegionRepo(db *gorm.DB) *RegionRepo {
	return &RegionRepo{db}
}

// Create attempts to create new `Region`
func (r *RegionRepo) Create(attrs *Region) (*Region, error) {
	if err := r.db.Create(attrs).Error; err != nil {
		return nil, err
	}
	return attrs, nil
}

// FindAll attempts to find all instances of `Region` by attributes
func (r *RegionRepo) FindAll(attrs *Region) ([]*Region, error) {
	var regions []*Region
	if err := r.db.Where(attrs).Find(&regions).Error; err != nil {
		return nil, err
	}
	return regions, nil
}

// FindOne attempts to find single instance of `Region` by attributes
func (r *RegionRepo) FindOne(attrs *Region) (*Region, error) {
	region := new(Region)
	if err := r.db.Where(attrs).First(region).Error; err != nil {
		return nil, err
	}
	return region, nil
}

// FindByID attempts to find single instance of `Region` by ID
func (r *RegionRepo) FindByID(id uint) (*Region, error) {
	region := new(Region)
	if err := r.db.First(region, id).Error; err != nil {
		return nil, err
	}
	return region, nil
}

// Update attempts to update `Region` with attributes
func (r *RegionRepo) Update(id uint, attrs *Region) (*Region, error) {
	region, err := r.FindByID(id)
	if err != nil {
		return nil, err
	}
	if err := r.db.Model(region).Updates(attrs).Error; err != nil {
		return nil, err
	}
	return region, nil
}

// Delete attempts to delete instance of `Region`
func (r *RegionRepo) Delete(id uint) (*Region, error) {
	region, err := r.FindByID(id)
	if err != nil {
		return nil, err
	}
	if err := r.db.Delete(region).Error; err != nil {
		return nil, err
	}
	return region, nil
}
