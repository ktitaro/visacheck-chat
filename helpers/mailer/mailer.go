package mailer

import (
	"io"
	"log"

	"github.com/ktitaro/visacheck/config"
	"gopkg.in/gomail.v2"
)

// Mailer represents mailer service
type Mailer struct {
	Config *config.MailerConfig
}

// New creates new instance of the `Mailer`
func New(config *config.MailerConfig) *Mailer {
	return &Mailer{config}
}

// Send attempts to send email
func (m *Mailer) Send(email *Email) error {
	if m.Config.Host == "" {
		s := gomail.SendFunc(func(from string, to []string, msg io.WriterTo) error {
			filler := "***************** EMAIL ******************"
			log.Printf("\n%s\n%s\n%s\n", filler, email, filler)
			return nil
		})

		if err := gomail.Send(s, email.message()); err != nil {
			return err
		}

		return nil
	}

	d := gomail.NewDialer(
		m.Config.Host,
		m.Config.Port,
		m.Config.Username,
		m.Config.Password,
	)

	if err := d.DialAndSend(email.message()); err != nil {
		return err
	}

	return nil
}
