package mailer

import (
	"bytes"
	"fmt"
	"strings"

	"gopkg.in/gomail.v2"
)

// Email represents email object
type Email struct {
	From        string
	To          []string
	Subject     string
	ContentType string
	Body        string
}

// message converts `Email` into `gomail.Message`
func (e *Email) message() *gomail.Message {
	message := gomail.NewMessage()
	message.SetHeader("To", e.To...)
	message.SetHeader("From", e.From)
	message.SetHeader("Subject", e.Subject)
	message.SetBody(
		e.ContentType,
		e.Body,
	)
	return message
}

// String converts `Email` into string
func (e *Email) String() string {
	var buffer bytes.Buffer
	buffer.WriteString(fmt.Sprintf("from: %s\n", e.From))
	buffer.WriteString(fmt.Sprintf("to: %s\n", strings.Join(e.To, ",")))
	buffer.WriteString(fmt.Sprintf("subject: %s\n", e.Subject))
	buffer.WriteString(fmt.Sprintf("content-type: %s\n", e.ContentType))
	buffer.WriteString(fmt.Sprintf("body: %s", e.Body))
	return buffer.String()
}
