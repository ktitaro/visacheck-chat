package serde

import "strconv"

// StringToUint attempts to convert string into uint
func StringToUint(input string) (uint, error) {
	inputUint64, err := strconv.ParseUint(input, 10, 32)
	if err != nil {
		return 0, err
	}
	inputUint := uint(inputUint64)
	return inputUint, nil
}
