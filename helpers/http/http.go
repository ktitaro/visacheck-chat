package http

import "github.com/gofiber/fiber"

// Ok represents JSON schema for HTTP 200
// used for a successfully processed request
func Ok(payload interface{}) *fiber.Map {
	schema := fiber.Map{
		"success": true,
		"status":  200,
	}
	if payload != nil {
		schema["payload"] = payload
	}
	return &schema
}

// BadRequest represents JSON schema for HTTP 400
func BadRequest(message interface{}) *fiber.Map {
	schema := fiber.Map{
		"success": false,
		"status":  "400",
	}
	if message != nil {
		schema["message"] = message
	} else {
		schema["message"] = "Bad Request"
	}
	return &schema
}

// NotFound represents JSON schema for HTTP 404
func NotFound(message interface{}) *fiber.Map {
	schema := fiber.Map{
		"success": false,
		"status":  "404",
	}
	if message != nil {
		schema["message"] = message
	} else {
		schema["message"] = "Not Found"
	}
	return &schema
}

// InternalServerError represents JSON shcema for HTTP 500
func InternalServerError(message interface{}) *fiber.Map {
	schema := fiber.Map{
		"success": false,
		"status":  "500",
	}
	if message != nil {
		schema["message"] = message
	} else {
		schema["message"] = "Internal Server Error"
	}
	return &schema
}
