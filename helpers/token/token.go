package token

import (
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/ktitaro/visacheck/config"
)

const (
	// AuthToken shortterm token used for authorization flow
	AuthToken = iota
	// SessionToken longterm token user for http sessions
	SessionToken
)

var (
	authTokenLifetime    = time.Minute * 5
	sessionTokenLifetime = time.Hour * 1

	lifetime = []time.Duration{
		authTokenLifetime,
		sessionTokenLifetime,
	}
)

// Token represents JWT tokens generator
type Token struct {
	config *config.TokenConfig
}

// New create new instance of the `Token`
func New(config *config.TokenConfig) *Token {
	return &Token{config}
}

// Generate attempts to generate new JWT token
func (t *Token) Generate(tokenType int, userID uint) (string, error) {
	claims := jwt.MapClaims{}
	claims["exp"] = time.Now().Add(lifetime[tokenType]).Unix()
	claims["user"] = userID

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tk, err := token.SignedString([]byte(t.config.Secret))
	if err != nil {
		return "", err
	}
	return tk, nil
}

// Extract attempts to extract user id from JWT token
func (t *Token) Extract(token *jwt.Token) (uint, error) {
	if claims, ok := token.Claims.(jwt.MapClaims); ok {
		if user, ok := claims["user"]; ok {
			if userID, ok := user.(float64); ok {
				userID := uint(userID)
				return userID, nil
			}
		}
	}
	err := fmt.Errorf("Token claims are invalid")
	return 0, err
}

// Validate attempts to validate JWT token
func (t *Token) Validate(tokenString string) (uint, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			err := fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			return nil, err
		}
		return []byte(t.config.Secret), nil
	})
	if err != nil {
		return 0, err
	}

	userID, err := t.Extract(token)
	if err != nil {
		return 0, err
	}
	return userID, nil
}
