package main

import (
	"fmt"
	"log"

	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/cors"
	"github.com/gofiber/fiber"
	"github.com/gofiber/fiber/middleware"
	jwtware "github.com/gofiber/jwt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"

	"github.com/ktitaro/visacheck/config"
	"github.com/ktitaro/visacheck/controllers"
	"github.com/ktitaro/visacheck/helpers/http"
	"github.com/ktitaro/visacheck/helpers/mailer"
	"github.com/ktitaro/visacheck/helpers/token"
	"github.com/ktitaro/visacheck/models"
)

func main() {
	config := config.Read()

	database, err := initDatabase(config.Database)
	if err != nil {
		log.Fatal(err)
	}
	defer database.Close()

	context := &controllers.Context{
		Repos:  models.New(database),
		Token:  token.New(config.Token),
		Mailer: mailer.New(config.Mailer),
	}

	if err := runServer(config, context); err != nil {
		log.Fatal(err)
	}
}

func initDatabase(config *config.DatabaseConfig) (*gorm.DB, error) {
	databaseURL := fmt.Sprintf(
		"host=%s port=%s dbname=%s user=%s password=%s sslmode=%s",
		config.Host,
		config.Port,
		config.Name,
		config.Username,
		config.Password,
		config.SSLMode,
	)

	conn, err := gorm.Open("postgres", databaseURL)
	if err != nil {
		return nil, err
	}

	migrations := conn.AutoMigrate(
		&models.User{},
		&models.Region{},
		&models.Country{},
		&models.Subregion{},
	)

	if err := migrations.Error; err != nil {
		return nil, err
	}

	return conn, nil
}

func runServer(config *config.Config, ctx *controllers.Context) error {
	server := fiber.New()

	server.Use(cors.New())
	server.Use(middleware.Logger())
	server.Use(middleware.Recover())
	server.Use(middleware.Compress())
	server.Use(middleware.RequestID())

	api := server.Group("/api")

	api.Get("/users", controllers.FetchUsers(ctx))
	api.Post("/users", controllers.CreateUser(ctx))
	api.Get("/users/:id", controllers.FetchUser(ctx))
	api.Patch("/users/:id", controllers.UpdateUser(ctx))
	api.Delete("/users/:id", controllers.DeleteUser(ctx))

	api.Get("/regions", controllers.FetchRegions(ctx))
	api.Post("/regions", controllers.CreateRegion(ctx))
	api.Get("/regions/:id", controllers.FetchRegion(ctx))
	api.Patch("/regions/:id", controllers.UpdateRegion(ctx))
	api.Delete("/regions/:id", controllers.DeleteRegion(ctx))

	api.Get("/subregions", controllers.FetchSubregions(ctx))
	api.Post("/subregions", controllers.CreateSubregion(ctx))
	api.Get("/subregions/:id", controllers.FetchRegion(ctx))
	api.Patch("/subregions/:id", controllers.UpdateRegion(ctx))
	api.Delete("/subregions/:id", controllers.DeleteRegion(ctx))

	api.Get("/countries", controllers.FetchCountries(ctx))
	api.Post("/countries", controllers.CreateCountry(ctx))
	api.Get("/countries/:id", controllers.FetchCountry(ctx))
	api.Patch("/countries/:id", controllers.UpdateCountry(ctx))
	api.Delete("/countries/:id", controllers.DeleteCountry(ctx))

	auth := server.Group("/auth")

	auth.Post("/login", controllers.Login(ctx))
	auth.Get("/verify", controllers.Verify(ctx))

	server.Use(jwtware.New(jwtware.Config{
		SigningKey: []byte(config.Token.Secret),
		ContextKey: "token",
	}))

	server.Get("/secret", func(c *fiber.Ctx) {
		token := c.Locals("token").(*jwt.Token)

		id, err := ctx.Token.Extract(token)
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		user, err := ctx.Repos.User.FindByID(id)
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		c.JSON(http.Ok(
			fmt.Sprintf("Welcome, %s", user.Email),
		))
	})

	return server.Listen(
		fmt.Sprintf("%s:%s",
			config.Server.Host,
			config.Server.Port,
		),
	)
}
