package controllers

import (
	"github.com/gofiber/fiber"
	"github.com/jinzhu/gorm"

	"github.com/ktitaro/visacheck/helpers/http"
	"github.com/ktitaro/visacheck/helpers/serde"
	"github.com/ktitaro/visacheck/models"
)

// CreateCountry attempts to create new country
func CreateCountry(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		attrs := new(models.Country)

		if err := c.BodyParser(attrs); err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		country, err := ctx.Repos.Country.Create(attrs)
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		c.JSON(http.Ok(country.Map()))
	}
}

// FetchCountries attemtps to get all countries
func FetchCountries(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		countries, err := ctx.Repos.Country.FindAll(&models.Country{})
		if err != nil {
			c.JSON(http.InternalServerError(nil))
			return
		}

		payload := []*fiber.Map{}

		for _, country := range countries {
			payload = append(payload, country.Map())
		}

		c.JSON(http.Ok(payload))
	}
}

// FetchCountry attempts to get country by id
func FetchCountry(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		id, err := serde.StringToUint(c.Params("id"))
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		country, err := ctx.Repos.Country.FindByID(id)
		if err != nil {
			if gorm.IsRecordNotFoundError(err) {
				c.JSON(http.NotFound(nil))
			} else {
				c.JSON(http.InternalServerError(nil))
			}
			return
		}

		c.JSON(http.Ok(country.Map()))
	}
}

// UpdateCountry attempts to update country by id
func UpdateCountry(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		id, err := serde.StringToUint(c.Params("id"))
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		attrs := new(models.Country)
		if err := c.BodyParser(attrs); err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		country, err := ctx.Repos.Country.Update(id, attrs)
		if err != nil {
			c.JSON(http.InternalServerError(nil))
			return
		}

		c.JSON(http.Ok(country.Map()))
	}
}

// DeleteCountry attempts to delete country by id
func DeleteCountry(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		id, err := serde.StringToUint(c.Params("id"))
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		country, err := ctx.Repos.Country.Delete(id)
		if err != nil {
			c.JSON(http.InternalServerError(nil))
			return
		}

		c.JSON(http.Ok(country.Map()))
	}
}
