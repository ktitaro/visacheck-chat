package controllers

import (
	"fmt"
	"log"

	"github.com/gofiber/fiber"

	"github.com/ktitaro/visacheck/helpers/http"
	"github.com/ktitaro/visacheck/helpers/mailer"
	"github.com/ktitaro/visacheck/helpers/token"
	"github.com/ktitaro/visacheck/models"
)

// Login attempts to initiate passwordless auth flow
func Login(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		type requestBody struct {
			Email string `json:"email"`
		}

		body := new(requestBody)
		if err := c.BodyParser(body); err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}
		if body.Email == "" {
			c.JSON(http.BadRequest(nil))
			return
		}

		user, err := ctx.Repos.User.FindOneOrCreate(&models.User{
			Email: body.Email,
		})
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		token, err := ctx.Token.Generate(token.AuthToken, user.ID)
		if err != nil {
			log.Println("HERE")
			log.Println(err)
			c.JSON(http.InternalServerError(nil))
			return
		}

		err = ctx.Mailer.Send(&mailer.Email{
			From:        "noreply@visacheck.ru",
			To:          []string{user.Email},
			Subject:     "Authorization Link",
			ContentType: "text/plain",
			Body: fmt.Sprintf(
				"Your authorization token: %s",
				token,
			),
		})
		if err != nil {
			c.JSON(http.InternalServerError(nil))
			return
		}

		c.JSON(http.Ok(nil))
	}
}

// Verify attempts to verify passwordless auth flow
func Verify(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		type requestBody struct {
			Token string `query:"token"`
		}

		body := new(requestBody)
		if err := c.BodyParser(body); err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		id, err := ctx.Token.Validate(body.Token)
		if err != nil {
			log.Println(err)
			c.JSON(http.BadRequest(nil))
			return
		}

		user, err := ctx.Repos.User.FindByID(id)
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		token, err := ctx.Token.Generate(token.SessionToken, user.ID)
		if err != nil {
			c.JSON(http.InternalServerError(nil))
			return
		}

		c.JSON(http.Ok(token))
	}
}
