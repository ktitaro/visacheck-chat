package controllers

import (
	"github.com/gofiber/fiber"

	"github.com/ktitaro/visacheck/helpers/mailer"
	"github.com/ktitaro/visacheck/helpers/token"
	"github.com/ktitaro/visacheck/models"
)

// Context represents handler's context
type Context struct {
	Repos  *models.Repos
	Token  *token.Token
	Mailer *mailer.Mailer
}

// Handler represents handler with context
type Handler func(ctx *Context) fiber.Handler
