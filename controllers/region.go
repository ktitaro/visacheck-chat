package controllers

import (
	"github.com/gofiber/fiber"
	"github.com/jinzhu/gorm"

	"github.com/ktitaro/visacheck/helpers/http"
	"github.com/ktitaro/visacheck/helpers/serde"
	"github.com/ktitaro/visacheck/models"
)

// CreateRegion attempts to create new region
func CreateRegion(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		attrs := new(models.Region)

		if err := c.BodyParser(attrs); err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		region, err := ctx.Repos.Region.Create(attrs)
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		c.JSON(http.Ok(region.Map()))
	}
}

// FetchRegions attemtps to get all regions
func FetchRegions(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		regions, err := ctx.Repos.Region.FindAll(&models.Region{})
		if err != nil {
			c.JSON(http.InternalServerError(nil))
			return
		}

		payload := []*fiber.Map{}

		for _, region := range regions {
			payload = append(payload, region.Map())
		}

		c.JSON(http.Ok(payload))
	}
}

// FetchRegion attempts to get region by id
func FetchRegion(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		id, err := serde.StringToUint(c.Params("id"))
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		region, err := ctx.Repos.Region.FindByID(id)
		if err != nil {
			if gorm.IsRecordNotFoundError(err) {
				c.JSON(http.NotFound(nil))
			} else {
				c.JSON(http.InternalServerError(nil))
			}
			return
		}

		c.JSON(http.Ok(region.Map()))
	}
}

// UpdateRegion attempts to update region by id
func UpdateRegion(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		id, err := serde.StringToUint(c.Params("id"))
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		attrs := new(models.Region)
		if err := c.BodyParser(attrs); err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		region, err := ctx.Repos.Region.Update(id, attrs)
		if err != nil {
			c.JSON(http.InternalServerError(nil))
			return
		}

		c.JSON(http.Ok(region.Map()))
	}
}

// DeleteRegion attempts to delete region by id
func DeleteRegion(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		id, err := serde.StringToUint(c.Params("id"))
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		region, err := ctx.Repos.Region.Delete(id)
		if err != nil {
			c.JSON(http.InternalServerError(nil))
			return
		}

		c.JSON(http.Ok(region.Map()))
	}
}
