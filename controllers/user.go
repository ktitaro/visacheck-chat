package controllers

import (
	"github.com/gofiber/fiber"
	"github.com/jinzhu/gorm"

	"github.com/ktitaro/visacheck/helpers/http"
	"github.com/ktitaro/visacheck/helpers/serde"
	"github.com/ktitaro/visacheck/models"
)

// CreateUser attempts to create new user
func CreateUser(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		attrs := new(models.User)

		if err := c.BodyParser(attrs); err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		user, err := ctx.Repos.User.Create(attrs)
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		c.JSON(http.Ok(user.Map()))
	}
}

// FetchUsers attemtps to get all users
func FetchUsers(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		users, err := ctx.Repos.User.FindAll(&models.User{})
		if err != nil {
			c.JSON(http.InternalServerError(nil))
			return
		}

		payload := []*fiber.Map{}

		for _, user := range users {
			payload = append(payload, user.Map())
		}

		c.JSON(http.Ok(payload))
	}
}

// FetchUser attempts to get user by id
func FetchUser(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		id, err := serde.StringToUint(c.Params("id"))
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		user, err := ctx.Repos.User.FindByID(id)
		if err != nil {
			if gorm.IsRecordNotFoundError(err) {
				c.JSON(http.NotFound(nil))
			} else {
				c.JSON(http.InternalServerError(nil))
			}
			return
		}

		c.JSON(http.Ok(user.Map()))
	}
}

// UpdateUser attempts to update user by id
func UpdateUser(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		id, err := serde.StringToUint(c.Params("id"))
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		attrs := new(models.User)
		if err := c.BodyParser(attrs); err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		user, err := ctx.Repos.User.Update(id, attrs)
		if err != nil {
			c.JSON(http.InternalServerError(nil))
			return
		}

		c.JSON(http.Ok(user.Map()))
	}
}

// DeleteUser attempts to delete user by id
func DeleteUser(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		id, err := serde.StringToUint(c.Params("id"))
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		user, err := ctx.Repos.User.Delete(id)
		if err != nil {
			c.JSON(http.InternalServerError(nil))
			return
		}

		c.JSON(http.Ok(user.Map()))
	}
}
