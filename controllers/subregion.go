package controllers

import (
	"github.com/gofiber/fiber"
	"github.com/jinzhu/gorm"

	"github.com/ktitaro/visacheck/helpers/http"
	"github.com/ktitaro/visacheck/helpers/serde"
	"github.com/ktitaro/visacheck/models"
)

// CreateSubregion attempts to create new subregion
func CreateSubregion(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		attrs := new(models.Subregion)

		if err := c.BodyParser(attrs); err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		subregion, err := ctx.Repos.Subregion.Create(attrs)
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		c.JSON(http.Ok(subregion.Map()))
	}
}

// FetchSubregions attemtps to get all subregions
func FetchSubregions(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		subregions, err := ctx.Repos.Subregion.FindAll(&models.Subregion{})
		if err != nil {
			c.JSON(http.InternalServerError(nil))
			return
		}

		payload := []*fiber.Map{}

		for _, subregion := range subregions {
			payload = append(payload, subregion.Map())
		}

		c.JSON(http.Ok(payload))
	}
}

// FetchSubregion attempts to get subregion by id
func FetchSubregion(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		id, err := serde.StringToUint(c.Params("id"))
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		subregion, err := ctx.Repos.Subregion.FindByID(id)
		if err != nil {
			if gorm.IsRecordNotFoundError(err) {
				c.JSON(http.NotFound(nil))
			} else {
				c.JSON(http.InternalServerError(nil))
			}
			return
		}

		c.JSON(http.Ok(subregion.Map()))
	}
}

// UpdateSubregion attempts to update subregion by id
func UpdateSubregion(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		id, err := serde.StringToUint(c.Params("id"))
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		attrs := new(models.Subregion)
		if err := c.BodyParser(attrs); err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		subregion, err := ctx.Repos.Subregion.Update(id, attrs)
		if err != nil {
			c.JSON(http.InternalServerError(nil))
			return
		}

		c.JSON(http.Ok(subregion.Map()))
	}
}

// DeleteSubregion attempts to delete subregion by id
func DeleteSubregion(ctx *Context) fiber.Handler {
	return func(c *fiber.Ctx) {
		id, err := serde.StringToUint(c.Params("id"))
		if err != nil {
			c.JSON(http.BadRequest(nil))
			return
		}

		subregion, err := ctx.Repos.Subregion.Delete(id)
		if err != nil {
			c.JSON(http.InternalServerError(nil))
			return
		}

		c.JSON(http.Ok(subregion.Map()))
	}
}
