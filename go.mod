module github.com/ktitaro/visacheck

go 1.14

require (
	github.com/cosmtrek/air v1.12.1 // indirect
	github.com/creack/pty v1.1.11 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fatih/color v1.9.0 // indirect
	github.com/gofiber/cors v0.2.1
	github.com/gofiber/csrf v0.2.0
	github.com/gofiber/fiber v1.12.4
	github.com/gofiber/jwt v0.1.1
	github.com/imdario/mergo v0.3.9 // indirect
	github.com/jinzhu/gorm v1.9.14
	github.com/klauspost/compress v1.10.10 // indirect
	github.com/pelletier/go-toml v1.8.0 // indirect
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
