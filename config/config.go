package config

// Config represents application configurations
type Config struct {
	Token    *TokenConfig
	Server   *ServerConfig
	Mailer   *MailerConfig
	Database *DatabaseConfig
}

// Read reads configurations into `Config`
func Read() *Config {
	readTokenConfig()
	readServerConfig()
	readMailerConfig()
	readDatabaseConfig()

	return &Config{
		Token:    newTokenConfig(),
		Server:   newServerConfig(),
		Mailer:   newMailerConfig(),
		Database: newDatabaseConfig(),
	}
}
