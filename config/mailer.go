package config

import (
	"os"
	"strconv"
)

var (
	mailerHost     string = ""
	mailerPort     int    = 25
	mailerUsername string = ""
	mailerPassword string = ""
)

// MailerConfig represents mailer config
type MailerConfig struct {
	Host     string
	Port     int
	Username string
	Password string
}

func readMailerConfig() {
	if val, ok := os.LookupEnv("MAILER_HOST"); ok {
		mailerHost = val
	}
	if val, ok := os.LookupEnv("MAILER_PORT"); ok {
		val, err := strconv.Atoi(val)
		if err != nil {
			panic(err)
		}
		mailerPort = val
	}
	if val, ok := os.LookupEnv("MAILER_USERNAME"); ok {
		mailerUsername = val
	}
	if val, ok := os.LookupEnv("MAILER_PASSWORD"); ok {
		mailerPassword = val
	}
}

func newMailerConfig() *MailerConfig {
	return &MailerConfig{
		Host:     mailerHost,
		Port:     mailerPort,
		Username: mailerUsername,
		Password: mailerPassword,
	}
}
