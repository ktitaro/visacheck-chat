package config

import (
	"os"
)

var (
	databaseHost     string = "127.0.0.1"
	databasePort     string = "5432"
	databaseName     string = "postgres"
	databaseSSLMode  string = "disable"
	databaseUsername string = "postgres"
	databasePassword string = "postgres"
)

// DatabaseConfig represents database config
type DatabaseConfig struct {
	Host     string
	Port     string
	Name     string
	SSLMode  string
	Username string
	Password string
}

func readDatabaseConfig() {
	if val, ok := os.LookupEnv("DATABASE_HOST"); ok {
		databaseHost = val
	}
	if val, ok := os.LookupEnv("DATABASE_PORT"); ok {
		databasePort = val
	}
	if val, ok := os.LookupEnv("DATABASE_NAME"); ok {
		databaseName = val
	}
	if val, ok := os.LookupEnv("DATABASE_SSL_MODE"); ok {
		databaseSSLMode = val
	}
	if val, ok := os.LookupEnv("DATABASE_USERNAME"); ok {
		databaseUsername = val
	}
	if val, ok := os.LookupEnv("DATABASE_PASSWORD"); ok {
		databasePassword = val
	}
}

func newDatabaseConfig() *DatabaseConfig {
	return &DatabaseConfig{
		Host:     databaseHost,
		Port:     databasePort,
		Name:     databaseName,
		SSLMode:  databaseSSLMode,
		Username: databaseUsername,
		Password: databasePassword,
	}
}
