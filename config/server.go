package config

import (
	"os"
)

var (
	serverHost string = "127.0.0.1"
	serverPort string = "8000"
)

// ServerConfig represents server config
type ServerConfig struct {
	Host string
	Port string
}

func readServerConfig() {
	if val, ok := os.LookupEnv("SERVER_HOST"); ok {
		serverHost = val
	}
	if val, ok := os.LookupEnv("SERVER_PORT"); ok {
		serverPort = val
	}
}

func newServerConfig() *ServerConfig {
	return &ServerConfig{
		Host: serverHost,
		Port: serverPort,
	}
}
