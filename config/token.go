package config

import "os"

var (
	tokenSecret = "secret"
)

// TokenConfig represents JWT tokens generator configuration
type TokenConfig struct {
	Secret string
}

func readTokenConfig() {
	if val, ok := os.LookupEnv("TOKEN_SECRET"); ok {
		tokenSecret = val
	}
}

func newTokenConfig() *TokenConfig {
	return &TokenConfig{
		Secret: tokenSecret,
	}
}
